import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PeriodicElement } from './app.component';

@Injectable()
export class AppService {

  constructor(private httpClient: HttpClient) { }

  getUserData(paginationObj): Observable<PeriodicElement[]> {
    console.log(JSON.parse(JSON.stringify(paginationObj)));
    return this.httpClient.post<PeriodicElement[]>('http://localhost:8083/getuserdata', paginationObj);
  }
  
}
