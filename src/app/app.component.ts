import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import { AppService } from './app.service';

export interface PeriodicElement {
  name: string;
  email: string;
  phone: number;
  degreeId: string;
  deptId: string;
  address: string;
}

export class Pagination {
  pageSize: number;
    pageIndex: number;
    sort?: SortData[];
    filter?: any;
}

export class SortData {
  active: string;
  direction: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
  {email: 'fgfg@gfg.gf', name: 'Hydrogen', phone: 1.0079, degreeId: 'H', deptId: 'FF', address: 'fff'},
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  displayedColumns: string[] = ['name', 'email', 'phone', 'degreeId', 'deptId', 'address'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  length: any = 0;
  pageSize: any = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageEvent: PageEvent = new PageEvent();
  paginationObj: Pagination = new Pagination();

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private appService: AppService)  {}

  ngOnInit() {
    this.pageEvent.pageSize = 10;
    this.pageEvent.pageIndex = 0;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.pagination();
  }

  setSort(sort: Sort) {
    let count = 0;
    if(this.paginationObj.sort) {
      this.paginationObj.sort.forEach(each => {
        if(each.active === sort.active) {
          each.direction = sort.direction;
        } else {
          count++;
        }
      });
      if(count === this.paginationObj.sort.length) {
        this.paginationObj.sort.push(sort);
      }
    } else {
      this.paginationObj.sort = [];
      this.paginationObj.sort.push(sort);
    }
    this.pagination();
  }

  pagination() {
    this.paginationObj.pageIndex = this.pageEvent.pageIndex;
    this.paginationObj.pageSize = this.pageEvent.pageSize;
    this.appService.getUserData(this.paginationObj).subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
    }, error => {

    })
  }
}
